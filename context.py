import json
from misc.util import log


class Context:

    def __init__(self):
        """Constructor."""
        with open("data/roletags.json", "r") as file:
            self._role_tags = json.load(file)
        with open("data/channeltags.json", "r") as file:
            self._channel_tags = json.load(file)

    def save_context(self):
        """Saves the context."""
        with open("data/roletags.json", "w") as file:
            json.dump(self._role_tags, file)
        with open("data/channeltags.json", "w") as file:
            json.dump(self._channel_tags, file)
        log("Context saved.")

    def get_tags_for_channel(self, channel):
        """Returns the channel tags of a given channel.

        Args:
            channel: The channel to obtain the tags for.
        """
        return [tag for tag in self._channel_tags.get(channel.id, [])]

    def add_tag_for_channel(self, channel, tag):
        """Adds a tag to a channel.

        Args:
            channel: The channel ID.
            tag: The tag to add.
        """
        if channel not in self._channel_tags:
            self._channel_tags[channel] = [tag]
        else:
            self._channel_tags[channel].append(tag)

    def del_tag_for_channel(self, channel, tag):
        """Removes a tag from a channel.

        Args:
            channel: The channel ID.
            tag: The tag to remove.
        """
        if channel in self._channel_tags:
            try:
                self._channel_tags[channel].remove(tag)
            except ValueError:
                pass

    def get_tags_for_role(self, role):
        """Returns the role tags of a given role.

        Args:
            role: The role to obtain the tags for.
        """
        return [tag for tag in self._role_tags.get(role.id, [])]

    def get_role_tags(self, user):
        """Returns the role tags of a given user.

        Args:
            user: The user to obtain the tags for.
        """
        tags = set()
        for role in user.roles:
            for tag in self.get_tags_for_role(role):
                tags.add(tag)
        return tags

    def add_tag_for_role(self, role, tag):
        """Adds a tag to a role.

        Args:
            role: The role ID.
            tag: The tag to add.
        """
        if role not in self._role_tags:
            self._role_tags[role] = [tag]
        else:
            self._role_tags[role].append(tag)

    def del_tag_for_role(self, role, tag):
        """Removes a tag from a role.

        Args:
            role: The role ID.
            tag: The tag to remove.
        """
        if role in self._role_tags:
            try:
                self._role_tags[role].remove(tag)
            except ValueError:
                pass

    @staticmethod
    def is_admin(user):
        """Checks admin status for a user.

        Args:
            user: The user to check.
        """
        if user.bot:
            return False

        for role in user.roles:
            if role.permissions.administrator:
                return True
        return False
