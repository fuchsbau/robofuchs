from context import Context
from misc.adapter import log
from modules.activation import ActivationModule
from modules.antispam import AntiSpamModule
from modules.archive import ArchiveModule
from modules.memberlogger import MemberLoggerModule
from modules.mines import MinesModule
from modules.mute import MuteModule
from modules.nuke import NukeModule
from modules.pushtotalk import PushToTalkModule
from modules.tag import TagModule
import discord


class Robofuchs:

    def __init__(self):
        self.bot = None
        self.server = None
        self.context = None
        self.modules = []

    async def on_ready(self):
        """Event handler for when the bot is ready."""
        log("Loading context...")
        self.context = Context()

        log("Initializing bot presence...")
        await self.bot.change_presence(game=discord.Game(name="Skynet"))

        log("Retrieving server...")
        for server in self.bot.servers:
            self.server = server
            log("Assigned main server: {0.name}".format(self.server))
            break

        if self.server.large:
            log("Requesting offline users on server...")
            await self.bot.request_offline_members(self.server)

        log("Loading modules...")
        self.modules = [
            ActivationModule(self, self.bot, self.context),
            AntiSpamModule(self, self.bot, self.context),
            ArchiveModule(self, self.bot, self.context),
            MemberLoggerModule(self, self.bot, self.context),
            MinesModule(self, self.bot, self.context),
            MuteModule(self, self.bot, self.context),
            NukeModule(self, self.bot, self.context),
            PushToTalkModule(self, self.bot, self.context),
            TagModule(self, self.bot, self.context)
        ]

        log("Initialization complete.")

    async def on_message_delete(self, msg):
        """Event handler for when a message is deleted.

        Args:
            msg: The message that was deleted.
        """
        for module in self.modules:
            await module.on_message_delete(msg)

    async def on_message_edit(self, before, after):
        """Event handler for when a message is edited.

        Args:
            before: The message before the edit.
            after: The message after the edit.
        """
        for module in self.modules:
            await module.on_message_edit(before, after)

    async def on_member_join(self, member):
        """Event handler for when a member joins the server.

        Args:
            member: The member that joined the server.
        """
        for module in self.modules:
            await module.on_member_join(member)

    async def on_member_remove(self, member):
        """Event handler for when a member leaves the server.

        Args:
            member: The member that left the server.
        """
        for module in self.modules:
            await module.on_member_remove(member)

    async def on_message(self, msg):
        """Event handler for messages.

        Args:
            msg: The message.
        """
        for module in self.modules:
            await module.on_message(msg)

    async def handle_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message that contains the command.
            cmd: The command.
            args: The arguments provided.
        """
        for module in self.modules:
            await module.on_command(msg, cmd, args)

    # async def cmd_die(self, msg, args):
    #     """Command that kills the bot.
    #
    #     Args:
    #         msg: The command message.
    #         args: The command arguments.
    #     """
    #     data = {
    #         "Veranlasser": str(msg.author)
    #     }
    #     embed = create_embed("Not-Aus", "Der Bot wird abgeschalten...",
    #                               0xAA0000, data)
    #     await self.bot.send_message(self.log_channel, embed=embed)
    #     await self.bot.logout()
    #     await asyncio.sleep(1)
    #     exit(0)
    #
    # async def cmd_mute(self, msg, args):
    #     """Command that mutes a user.
    #
    #     Args:
    #         msg: The command message.
    #         args: The command arguments.
    #     """
    #     targets = msg.mentions
    #     if len(targets) == 0:
    #         embed = create_embed("Fehler", "Keine Ziele", 0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     if any(map(self.is_team_member, targets)):
    #         embed = create_embed("Fehler", "Operation nicht gestattet",
    #                                   0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     for target in targets:
    #         await self.bot.replace_roles(target, self.mute_role)
    #
    #     names = ", ".join(map(str, targets))
    #
    #     data = {
    #         "Veranlasser": str(msg.author)
    #     }
    #     fmt = "Benutzer wurden geknebelt: {0}"
    #     embed = create_embed("Benutzer geknebelt", fmt.format(names),
    #                               0x00AA00, data)
    #     await self.bot.send_message(self.log_channel, embed=embed)
    #
    # async def cmd_purge(self, msg, args):
    #     """Command that purges messages.
    #
    #     Args:
    #         msg: The command message.
    #         args: The command arguments.
    #     """
    #     targets = msg.mentions
    #     if len(targets) == 0:
    #         embed = create_embed("Fehler", "Keine Ziele", 0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     channels = msg.channel_mentions
    #     if len(channels) == 0:
    #         embed = create_embed("Fehler", "Keine Channel", 0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     if any(map(self.is_protected, channels)):
    #         embed = create_embed("Fehler", "Operation nicht gestattet",
    #                                   0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     def is_target(msg):
    #         return msg.author in targets
    #
    #     # Prevent deletion spam.
    #     self.purge_active = True
    #     for channel in channels:
    #         await self.bot.purge_from(channel, check=is_target)
    #     await asyncio.sleep(2)
    #     self.purge_active = False
    #
    #     data = {
    #         "Veranlasser": str(msg.author),
    #         "Channel": ", ".join(map(lambda c: c.name, channels))
    #     }
    #     names = ", ".join(map(str, targets))
    #     fmt = ("Eine Massenlöschung wurde für die folgenden Ziele "
    #            + "durchgeführt: {0}".format(names))
    #     embed = create_embed("Massenlöschung", fmt, 0xAA00AA, data)
    #     await self.bot.send_message(self.log_channel, embed=embed)
    #
    # async def cmd_say(self, msg, args):
    #     """Command that sends an embed.
    #
    #     Args:
    #         msg: The command message.
    #         args: The command arguments.
    #     """
    #     title = []
    #     desc = []
    #     color = 0
    #     stop = 0
    #     for arg in args:
    #         if arg == "STOP":
    #             stop += 1
    #             continue
    #         if stop == 0:
    #             title.append(arg)
    #         elif stop == 1:
    #             color = int(arg, 16)
    #         elif stop == 2:
    #             desc.append(arg)
    #     title = " ".join(title)
    #     desc = " ".join(desc)
    #
    #     embed = create_embed(title, desc, color)
    #     await self.bot.send_message(msg.channel, embed=embed)
    #
    # async def cmd_unmute(self, msg, args):
    #     """Command that unmutes a user.
    #
    #     Args:
    #         msg: The command message.
    #         args: The command arguments.
    #     """
    #     targets = msg.mentions
    #     if len(targets) == 0:
    #         embed = create_embed("Fehler", "Keine Ziele", 0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     if any(map(self.is_team_member, targets)):
    #         embed = create_embed("Fehler", "Operation nicht gestattet",
    #                                   0xAA0000)
    #         await self.bot.send_message(msg.channel, embed=embed)
    #         return
    #
    #     for target in targets:
    #         await self.bot.replace_roles(target, self.user_role)
    #
    #     names = ", ".join(map(str, targets))
    #
    #     data = {
    #         "Veranlasser": str(msg.author)
    #     }
    #     fmt = "Benutzer wurden entknebelt: {0}"
    #     embed = create_embed("Benutzer entknebelt", fmt.format(names),
    #                               0x00AA00, data)
    #     await self.bot.send_message(self.log_channel, embed=embed)
