from misc.util import log
from modules.module import Module
import re


class AntiSpamModule(Module):

    _blocked_messages = [
        re.compile(".*(.)\\1{4,}.*")  # Character repetition (5+)
    ]

    _spam_cache = dict()

    async def on_message(self, msg):
        """Event handler for messages.

        Args:
            msg: The message.
        """
        if "nospamfilter" in self._context.get_role_tags(msg.author):
            return

        text = msg.clean_content

        for pattern in self._blocked_messages:
            if pattern.match(text) is not None:
                log("Deleted message by {0}: Pattern"
                    .format(msg.author))
                await self._bot.delete_message(msg)
                return

        if msg.author.id in self._spam_cache and len(text) >= 1:
            if text.lower().startswith(self._spam_cache[msg.author.id]):
                log("Deleted message by {0}: Repetition"
                    .format(msg.author))
                await self._bot.delete_message(msg)
                return

        self._spam_cache[msg.author.id] = text.lower()[:20]
