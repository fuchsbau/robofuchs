from discord import ChannelType
from modules.module import Module
from misc.util import create_embed, log


help_texts = {
    "base": "`.tags roles` -- Rollentags\n"
            "`.tags channels` -- Channeltags",
    "roles": "`.tags roles list` -- Tags auflisten\n"
             "`.tags roles add <id> <tag>` -- Tag hinzufügen\n"
             "`.tags roles del <id> <tag>` -- Tag löschen",
    "channels": "`.tags channels list` -- Tags auflisten\n"
                "`.tags channels add <id> <tag>` -- Tag hinzufügen\n"
                "`.tags channels del <id> <tag>` -- Tag löschen"
}


class TagModule(Module):

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        if cmd != "tags":
            return

        if not self._context.is_admin(msg.author):
            log("Forbidden: .tags for {0}".format(msg.author))
            return

        if len(args) == 0:
            embed = create_embed(".tags", help_texts["base"], 0x00FFFF)
            await self._bot.send_message(msg.channel, embed=embed)
        elif args[0] == "roles":
            await self.subcommand_roles(msg, args[1:])
        elif args[0] == "channels":
            await self.subcommand_channels(msg, args[1:])
        else:
            embed = create_embed(".tags", "Ungültiges Kommando", 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)

    async def subcommand_roles(self, msg, args):
        """Event handler for the roles-subcommand.

        Args:
            msg: The message.
            args: The remaining arguments.
        """
        if len(args) == 0:
            embed = create_embed(".tags roles", help_texts["roles"], 0x00FFFF)
            await self._bot.send_message(msg.channel, embed=embed)
        elif args[0] == "list":
            roletexts = ["__**Rollentags**__"]
            for role in self._frontend.server.roles:
                tags = sorted(self._context.get_tags_for_role(role))
                tags = map(lambda t: "`{0}`".format(t), tags)
                tags = ", ".join(tags)
                name = role.name if not role.is_everyone else "Everyone"
                roletexts.append(
                    "**{2}** ({0.id}): {1}".format(role, tags, name))
            await self.send_line_data(msg.channel, roletexts)
        elif args[0] in ("add", "del"):
            await self.subcommand_roles_modify(msg, args[1:], args[0])
        else:
            embed = create_embed(".tags roles", "Ungültiges Kommando", 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)

    async def subcommand_roles_modify(self, msg, args, op):
        """Event handler for the roles-subcommands for modification.

        Args:
            msg: The message.
            args: The remaining arguments.
            op: The operation.
        """
        if len(args) != 2:
            embed = create_embed(".tags roles", "Ungültiges Kommando", 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        role_ids = set(role.id for role in self._frontend.server.roles)
        if args[0] not in role_ids:
            embed = create_embed(".tags roles", "Ungültige Rolle", 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        if op == "add":
            self._context.add_tag_for_role(args[0], args[1])
        elif op == "del":
            self._context.del_tag_for_role(args[0], args[1])
        self._context.save_context()

        embed = create_embed(".tags roles", "Tags geändert", 0x00FF00)
        await self._bot.send_message(msg.channel, embed=embed)

    async def subcommand_channels(self, msg, args):
        """Event handler for the channels-subcommand.

        Args:
            msg: The message.
            args: The remaining arguments.
        """
        if len(args) == 0:
            embed = create_embed(".tags channels", help_texts["channels"],
                                 0x00FFFF)
            await self._bot.send_message(msg.channel, embed=embed)
        elif args[0] == "list":
            channeltexts = ["__**Channeltags**__"]
            for channel in self._frontend.server.channels:
                if channel.type != ChannelType.text:
                    continue
                tags = sorted(self._context.get_tags_for_channel(channel))
                tags = map(lambda t: "`{0}`".format(t), tags)
                tags = ", ".join(tags)
                channeltexts.append(
                    "**{0.name}** ({0.id}): {1}".format(channel, tags))
            await self.send_line_data(msg.channel, channeltexts)
        elif args[0] in ("add", "del"):
            await self.subcommand_channels_modify(msg, args[1:], args[0])
        else:
            embed = create_embed(".tags channels", "Ungültiges Kommando",
                                 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)

    async def subcommand_channels_modify(self, msg, args, op):
        """Event handler for the channels-subcommands for modification.

        Args:
            msg: The message.
            args: The remaining arguments.
            op: The operation.
        """
        if len(args) != 2:
            embed = create_embed(".tags channels", "Ungültiges Kommando",
                                 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        channel_ids = set(channel.id
                          for channel in self._frontend.server.channels)
        if args[0] not in channel_ids:
            embed = create_embed(".tags channels", "Ungültiger Kanal", 0xFF0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        if op == "add":
            self._context.add_tag_for_channel(args[0], args[1])
        elif op == "del":
            self._context.del_tag_for_channel(args[0], args[1])
        self._context.save_context()

        embed = create_embed(".tags channels", "Tags geändert", 0x00FF00)
        await self._bot.send_message(msg.channel, embed=embed)

    async def send_line_data(self, channel, data):
        """Sends line-data whilst respecting the 2000 character limit.

        Args:
            channel: The channel to send to.
            data: The sent data.
        """
        text = ""
        for entry in data:
            if len(text) + len(entry) + 1 <= 2000:
                text += "\n" + entry
            else:
                await self._bot.send_message(channel, text)
                text = entry
        if len(text) > 0:
            await self._bot.send_message(channel, text)
