from misc.util import create_embed, log
from modules.module import Module


class MemberLoggerModule(Module):

    async def on_member_join(self, member):
        """Event handler for member join.

        Args:
            member: The member.
        """
        data = {
            "Handle": str(member)
        }
        embed = create_embed("Benutzer betritt Server", "Ein Benutzer hat den "
                                                        "Server betreten!",
                             0x55FF55, data)
        await self.log_embed(embed)

    async def on_member_remove(self, member):
        """Event handler for member removal.

        Args:
            member: The member.
        """
        data = {
            "Handle": str(member)
        }
        embed = create_embed("Benutzer verlässt Server", "Ein Benutzer hat den "
                                                         "Server verlassen!",
                             0xFF5555, data)
        await self.log_embed(embed)
