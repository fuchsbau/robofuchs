from misc.util import log
from modules.module import Module
from random import randint


class MinesModule(Module):

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        if cmd != "mines":
            return

        if "mines" not in self._context.get_role_tags(msg.author):
            log("Forbidden: .mines for {0}".format(msg.author))
            return

        if len(args) != 2:
            await self._bot.send_message(msg.channel,
                                         "**Syntax:** .mines <size> <num>")
            return

        try:
            n = int(args[0])
            k = int(args[1])
        except ValueError:
            return

        field = self.create_field(n, k)
        field = self.convert_field(field)

        res = "**Wer sucht, der findet; wer drauftritt, " \
              "verschwindet! (n={0},k={1})**".format(n, k)
        for line in field:
            res += "\n"
            for entry in line:
                res += entry

        await self._bot.send_message(msg.channel, res)
    
    @staticmethod
    def convert_field(field):
        """Converts a minesweeper field to a discord-field.
        
        Args:
            field: The field to convert.
        """
        replace_dict = {
            "x": "||:boom:||",
            "1": "||:one:||",
            "2": "||:two:||",
            "3": "||:three:||",
            "4": "||:four:||",
            "5": "||:five:||",
            "6": "||:six:||",
            "7": "||:seven:||",
            "8": "||:eight:||",
            "9": "||:nine:||",
            "0": "||:zero:||",
        }
        for x in range(len(field)):
            for y in range(len(field[x])):
                field[x][y] = replace_dict.get(field[x][y], field[x][y])
        return field

    @staticmethod
    def create_field(n, k):
        """Creates a minesweeper field as a two-dimensional list.

        Args:
            n: The side length of the square field.
            k: The number of mines.
        """
        if n > 10:
            n = 10
        if k > n * n / 2:
            k = n * n / 2

        # Generate mines.
        res = [[None for _ in range(n)] for _ in range(n)]
        for _ in range(k):
            while True:
                x = randint(0, n - 1)
                y = randint(0, n - 1)
                if res[x][y] is None:
                    res[x][y] = "x"
                    break

        # Generate numbers.
        for x in range(n):
            for y in range(n):
                if res[x][y] == "x":
                    continue
                n_bombs = 0
                for xoff in (-1, 0, 1):
                    for yoff in (-1, 0, 1):
                        xr = x + xoff
                        yr = y + yoff
                        if xr < 0 or xr >= n or yr < 0 or yr >= n:
                            continue
                        if res[xr][yr] == "x":
                            n_bombs += 1
                # noinspection PyTypeChecker
                res[x][y] = str(n_bombs)
        return res
