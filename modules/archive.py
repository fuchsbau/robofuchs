from misc.util import create_embed, dateformat, log
from modules.module import Module


class ArchiveModule(Module):

    async def on_message_delete(self, msg):
        """Event handler for message deletion.

        Args:
            msg: The message.
        """
        if msg.author.bot:
            return

        if "noarchive" in self._context.get_tags_for_channel(msg.channel):
            return

        data = {
            "Autor": str(msg.author),
            "Channel": msg.channel.name,
            "Sendezeit (UTC)": dateformat(msg.timestamp)
        }
        embed = create_embed("Nachricht gelöscht", msg.content, 0xAA0000, data)
        await self.log_embed(embed)

    async def on_message_edit(self, msg, msgnew):
        """Event handler for message editing.

        Args:
            msg: The old message.
            msgnew: The new message.
        """
        if msg.author.bot:
            return

        if "noarchive" in self._context.get_tags_for_channel(msg.channel):
            return

        data = {
            "Autor": str(msg.author),
            "Channel": msg.channel.name,
            "Sendezeit (UTC)": dateformat(msg.timestamp)
        }
        embed = create_embed("Nachricht bearbeitet", msg.content, 0xFFAA00,
                             data)
        await self.log_embed(embed)
