from misc.util import create_embed, log
from modules.module import Module


class MuteModule(Module):

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        if cmd != "mute":
            return

        if "moderator" not in self._context.get_role_tags(msg.author):
            log("Forbidden: .mute for {0}".format(msg.author))
            return

        targets = msg.mentions
        if len(targets) == 0:
            embed = create_embed("Fehler", "Keine Ziele", 0xAA0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        if any(map(lambda t: "moderator" in self._context.get_role_tags(t),
                   targets)):
            embed = create_embed("Fehler", "Operation nicht gestattet",
                                 0xAA0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        mute_role = self.get_first_tagged_role("mute")
        if mute_role is None:
            log("Unknown mute role!")
            return
        user_role = self.get_first_tagged_role("default")
        if user_role is None:
            log("Unknown user role!")
            return

        mute_targets = []
        unmute_targets = []
        for target in targets:
            if mute_role in target.roles:
                await self._bot.replace_roles(target, user_role)
                unmute_targets.append(target)
            else:
                await self._bot.replace_roles(target, mute_role)
                mute_targets.append(target)

        data = {
            "Veranlasser": str(msg.author)
        }
        if len(mute_targets) > 0:
            mute_names = ", ".join(map(str, mute_targets))
            fmt = "Benutzer wurden geknebelt: {0}".format(mute_names)
            embed = create_embed("Knebel", fmt, 0x0000AA, data)
            await self.log_embed(embed)
        if len(unmute_targets) > 0:
            unmute_names = ", ".join(map(str, unmute_targets))
            fmt = "Benutzer wurden entknebelt: {0}".format(unmute_names)
            embed = create_embed("Knebel", fmt, 0x0000AA, data)
            await self.log_embed(embed)
