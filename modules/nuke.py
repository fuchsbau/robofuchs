from misc.util import log
from modules.module import Module
from random import randint


class NukeModule(Module):

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        if cmd != "nuke":
            return

        if not self._context.is_admin(msg.author):
            log("Forbidden: .nuke for {0}".format(msg.author))
            return

        targets = set(msg.mentions)
        indiscriminate = len(targets) == 0
        channels = msg.channel_mentions

        # Determine nuking targets.
        to_nuke = []
        for channel in channels:
            if "nonuke" in self._context.get_tags_for_channel(channel):
                continue

            logs = self._bot.logs_from(channel, limit=512)
            async for message in logs:
                if indiscriminate or message.author in targets:
                    to_nuke.append(message)

        # Request lock-in confirmation.
        token = str(randint(1000, 9999))
        prompt = "Locking in on {0} messages. Type {1} to nuke.".format(
            len(to_nuke), token)
        await self._bot.send_message(msg.channel, prompt)
        confirm = await self._bot.wait_for_message(author=msg.author, timeout=5,
                                                   channel=msg.channel,
                                                   content=token)

        # Check the confirmation.
        if confirm is not None:
            # Nuke the messages.
            for msg in to_nuke:
                await self._bot.delete_message(msg)
            ack = "Nuked {0} messages.".format(len(to_nuke))
            await self._bot.send_message(msg.channel, ack)
        else:
            nack = "Confirmation time out."
            await self._bot.send_message(msg.channel, nack)
