from misc.util import create_embed, log
from modules.module import Module


class PushToTalkModule(Module):

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        if cmd != "ptt":
            return

        if "moderator" not in self._context.get_role_tags(msg.author):
            log("Forbidden: .ptt for {0}".format(msg.author))
            return

        targets = msg.mentions
        if len(targets) == 0:
            embed = create_embed("Fehler", "Keine Ziele", 0xAA0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        if any(map(lambda t: "moderator" in self._context.get_role_tags(t),
                   targets)):
            embed = create_embed("Fehler", "Operation nicht gestattet",
                                 0xAA0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        ptt_role = self.get_first_tagged_role("pushtotalk")
        if ptt_role is None:
            log("Unknown push to talk role!")
            return

        ptt_targets = []
        unptt_targets = []
        for target in targets:
            if ptt_role in target.roles:
                await self._bot.remove_roles(target, ptt_role)
                unptt_targets.append(target)
            else:
                await self._bot.add_roles(target, ptt_role)
                ptt_targets.append(target)

        data = {
            "Veranlasser": str(msg.author)
        }
        if len(ptt_targets) > 0:
            ptt_names = ", ".join(map(str, ptt_targets))
            fmt = "Benutzern wurde PTT aktiviert: {0}".format(ptt_names)
            embed = create_embed("Push-To-Talk", fmt, 0xAA00AA, data)
            await self.log_embed(embed)
        if len(unptt_targets) > 0:
            unptt_names = ", ".join(map(str, unptt_targets))
            fmt = "Benutzern wurde PTT deaktiviert: {0}".format(unptt_names)
            embed = create_embed("Push-To-Talk", fmt, 0xAA00AA, data)
            await self.log_embed(embed)
