from misc.util import create_embed, log
from modules.module import Module


class ActivationModule(Module):

    async def on_member_join(self, member):
        """Event handler for member join.

        Args:
            member: The member.
        """
        activation_role = self.get_first_tagged_role("activation")
        if activation_role is None:
            log("Unknown activation role!")
            return

        await self._bot.add_roles(member, activation_role)

        activator_role = self.get_first_tagged_role("activator")
        if activator_role is None:
            log("Unknown activator role!")
            return

        activation_channel = self.get_first_tagged_channel("activation")
        if activation_channel is None:
            log("Unknown activation channel!")
            return

        fmt = "Willkommen {0.name} bei den Fuchspfoten! Ein Teammitglied " \
              "wird sich in Kürze um Deine Aktivierung kümmern!"

        tagline = "{0} {1}".format(member.mention, activator_role.mention)
        embed = create_embed("Willkommen bei den Fuchspfoten!",
                             fmt.format(member), 0x00AA00)
        await self._bot.send_message(activation_channel, tagline, embed=embed)

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        if cmd != "activate":
            return

        if "activator" not in self._context.get_role_tags(msg.author):
            log("Forbidden: .activate for {0}".format(msg.author))
            return

        targets = msg.mentions
        if len(targets) == 0:
            embed = create_embed("Fehler", "Keine Ziele", 0xAA0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        if any(map(lambda t: "activation" not in self._context.get_role_tags(t),
                   targets)):
            embed = create_embed("Fehler", "Operation nicht gestattet",
                                 0xAA0000)
            await self._bot.send_message(msg.channel, embed=embed)
            return

        user_role = self.get_first_tagged_role("default")
        if user_role is None:
            log("Unknown user role!")
            return

        for target in targets:
            await self._bot.replace_roles(target, user_role)

        names = ", ".join(map(str, targets))
        data = {
            "Veranlasser": str(msg.author)
        }
        fmt = "Benutzer wurden aktiviert: {0}".format(names)
        embed = create_embed("Benutzeraktivierung", fmt, 0x00AA00, data)
        await self.log_embed(embed)
