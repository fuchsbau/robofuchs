from discord import ChannelType
from misc.util import log


class Module:

    def __init__(self, frontend, bot, context):
        """Constructor."""
        self._frontend = frontend
        self._bot = bot
        self._context = context

    async def on_message(self, msg):
        """Event handler for messages.

        Args:
            msg: The message.
        """
        pass

    async def on_message_delete(self, msg):
        """Event handler for message deletion.

        Args:
            msg: The message.
        """
        pass

    async def on_message_edit(self, msg, msgnew):
        """Event handler for message editing.

        Args:
            msg: The old message.
            msgnew: The new message.
        """
        pass

    async def on_member_join(self, member):
        """Event handler for member join.

        Args:
            member: The member.
        """
        pass

    async def on_member_remove(self, member):
        """Event handler for member removal.

        Args:
            member: The member.
        """
        pass

    async def on_command(self, msg, cmd, args):
        """Event handler for commands.

        Args:
            msg: The message.
            cmd: The command label.
            args: The command arguments.
        """
        pass

    def get_first_tagged_channel(self, tag):
        """Obtains the first channel with the given tag, or None.

        Args:
            tag: The channel tag.
        """
        for channel in self._frontend.server.channels:
            if channel.type != ChannelType.text:
                continue
            if tag in self._context.get_tags_for_channel(channel):
                return channel
        return None

    def get_first_tagged_role(self, tag):
        """Obtains the first role with the given tag, or None.

        Args:
            tag: The role tag.
        """
        for role in self._frontend.server.roles:
            if tag in self._context.get_tags_for_role(role):
                return role
        return None

    async def log_embed(self, embed):
        """Logs an embed in the first log-tagged channel, if such a channel
        exists.

        Args:
            embed: The embed.
        """
        log_channel = self.get_first_tagged_channel("log")
        if log_channel is None:
            log("Unknown log channel!")
            return
        await self._bot.send_message(log_channel, embed=embed)
