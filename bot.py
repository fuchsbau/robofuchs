from misc.adapter import create_bot, connect_bot
from robofuchs import Robofuchs

if __name__ == "__main__":
    rf = Robofuchs()
    bot = create_bot(rf)
    rf.bot = bot
    connect_bot(bot)
